from flask import Flask

def create_app():
    app = Flask(__name__)
    #todo should be stored separately
    app.config['SECRET_KEY'] = 'supersupersecretkey'

    from .views import views

    app.register_blueprint(views, url_prefix='/')

    return app