FROM python

WORKDIR /python-website

COPY requirements.txt requirements.txt

RUN 

RUN pip3 install -r requirements.txt

COPY . /python-website

EXPOSE 5000

ENV FLASK_APP=main.py

CMD [ "python3", "-m", "flask", "run", "--host=0.0.0.0"]